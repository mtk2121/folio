class AddUserIdToResolutions < ActiveRecord::Migration
  def change
    add_column :resolutions, :user_id, :integer
    add_index :resolutions, :user_id
  end
end
