# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Folio::Application.config.secret_key_base = 'da0390dead2db9c321e5f479ba4fd75eb9444485b2ce9e4674aaf8b2b2ff615a721a719946493d928ae51f1557a128f3da46aff02d348e6d6fe6c3250245059f'
