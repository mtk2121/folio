json.array!(@resolutions) do |resolution|
  json.extract! resolution, :id, :title, :description
  json.url resolution_url(resolution, format: :json)
end
